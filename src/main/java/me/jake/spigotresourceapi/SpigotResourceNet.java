package me.jake.spigotresourceapi;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by Jake on 1/5/2021.
 */
public class SpigotResourceNet {
    static String getJson(int resourceId){
        try {
            HttpURLConnection get = GET(resourceId);
            int rcode = get.getResponseCode();
//            System.err.println("Response code: " + rcode);
            if(rcode == 404){
                return null;
            }else if(rcode == 503){
                return "T";
            }
            return IOUtils.toString(get.getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private static HttpURLConnection GET(int request) throws IOException {
        URL url = new URL("https://api.spigotmc.org/simple/0.1/index.php?action=getResource&id=" + request);
        HttpURLConnection openConnection = (HttpURLConnection) url.openConnection();
        openConnection.setRequestMethod("GET");
        openConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
        openConnection.setRequestProperty("User-Agent", "NING/1.0");
        return openConnection;
    }

}
